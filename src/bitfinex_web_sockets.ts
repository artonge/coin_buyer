import * as ws from "ws"
import { Subject, Observable } from "rxjs"

const CHANNEL_TO_SYMBOL_MAP: { [channelId: number]: string } = {}
const SYMBOL_TO_OBSERVABLE_MAP: { [symbol: string]: Subject<number> } = {}

// Open web socket - https://docs.bitfinex.com/v2/reference#ws-public-ticker
const BIT_WS = new ws("wss://api.bitfinex.com/ws/2")
const BIT_WS_PROMISE = new Promise(resolve => BIT_WS.on("open", resolve))

// Clean exit for nodemon
process.once("SIGUSR2", () => {
	console.log("Closing web socket")
	// Close the web socket
	BIT_WS.close()
	// Kill itself
	process.kill(process.pid, "SIGUSR2")
})

// Listen for messages
BIT_WS.on("message", msg => {
	const parsedMsg = JSON.parse(msg.toString())

	// Listen for messages
	if (Array.isArray(parsedMsg)) {
		// If it's an array, it a price update
		if (parsedMsg[1] === "hb") {
			return // Return on heartbeat
		}

		// Update coin's maxPrice if needed
		const channelId = parsedMsg[0]
		const newPrice = parsedMsg[1][6]
		const symbol = CHANNEL_TO_SYMBOL_MAP[channelId]
		SYMBOL_TO_OBSERVABLE_MAP[symbol].next(newPrice)
	} else if (typeof parsedMsg === "object") {
		// If it's an object it's an event
		switch (parsedMsg.event) {
			case "info":
				console.log(`Openning web socket...`)
				break

			case "subscribed": {
				const symbol = (parsedMsg.pair as string).split("USD")[0]
				const channelId = parsedMsg.chanId

				console.log(`Subscribed to channel ${channelId} for ${symbol}`)
				CHANNEL_TO_SYMBOL_MAP[channelId] = symbol
				break
			}

			case "error":
				// console.log(parsedMsg)
				const symbol = (parsedMsg.pair as string).split("USD")[0]
				console.error(`Error subscribing for ${symbol}: ${parsedMsg.msg}`)
				break

			default:
				// Don't now what event is it so log
				console.warn("Unknown event:", parsedMsg)
		}
	} else {
		// Don't now what it is so log it
		console.warn("Unknown message type:", parsedMsg)
	}
})

export function bitfinex(symbol: string): Observable<number> {
	subscribeToCoinPriceUpdate(symbol)
	SYMBOL_TO_OBSERVABLE_MAP[symbol] = new Subject()
	return SYMBOL_TO_OBSERVABLE_MAP[symbol].asObservable()
}

async function subscribeToCoinPriceUpdate(symbol: string) {
	// Await for web socket to open
	await BIT_WS_PROMISE

	// Send the subscribe event to bitfinex
	BIT_WS.send(
		JSON.stringify({
			event: "subscribe",
			channel: "ticker",
			symbol: `t${symbol}USD`,
		}),
	)
}
