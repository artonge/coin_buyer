import { IMyCoin } from "./models"
import { handlePriceChange } from "./lib"

describe("handlePriceChange", () => {
	let BTC: IMyCoin

	beforeEach(() => {
		BTC = {
			info: { Symbol: "BTC" },
			maxPrice: 1000,
		} as IMyCoin
	})

	it("Should set max price to newPrice when newPrice is greater than maxPrice", () => {
		handlePriceChange(BTC, 1100, 500, [])
		expect(BTC.maxPrice).toBe(1100)
	})

	describe("No holdings", () => {
		it("Should not buy when current price is lower than max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 900, 500, [])
			expect(wallet).toBe(500)
			expect(holdings).toEqual([])
		})

		it("Should buy when current price is the same as max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 1000, 500, [])
			expect(wallet).toBe(400)
			expect(holdings).toEqual([
				{
					symbol: "BTC",
					amount: 100,
					buyPrice: 1000,
				},
			])
		})

		it("Should buy when current price is greater than max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 1100, 500, [])
			expect(wallet).toBe(400)
			expect(holdings).toEqual([
				{
					symbol: "BTC",
					amount: 100,
					buyPrice: 1100,
				},
			])
		})

		it("Should buy when current price is greater than max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 1100, 50, [])
			expect(wallet).toBe(0)
			expect(holdings).toEqual([
				{
					symbol: "BTC",
					amount: 50,
					buyPrice: 1100,
				},
			])
		})
	})

	describe("One holding for the current coin", () => {
		const HOLDINGS = [
			{
				symbol: "BTC",
				amount: 100,
				buyPrice: 1000,
			},
		]

		xit("Should not buy when current price is lower than max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 900, 500, HOLDINGS)
			expect(wallet).toBe(500)
			expect(holdings).toEqual(HOLDINGS)
		})

		it("Should not buy when current price is the same as max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 1000, 500, HOLDINGS)
			expect(wallet).toBe(500)
			expect(holdings).toEqual(HOLDINGS)
		})

		it("Should not buy when current price is greater than max price", () => {
			const [wallet, holdings] = handlePriceChange(BTC, 1100, 500, HOLDINGS)
			expect(wallet).toBe(500)
			expect(holdings).toEqual(HOLDINGS)
		})
	})
})
