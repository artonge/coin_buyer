import { IMyCoin, IHolding } from "./models"

export function handlePriceChange(
	coin: IMyCoin,
	newPrice: number,
	wallet: number,
	holdings: IHolding[],
): [number, IHolding[]] {
	// console.log(`New price for ${coin.info.Symbol}: ${newPrice}$`)
	// console.log(coin.histo)
	// If new price is greater than maxPrice then update maxPrice
	if (coin.maxPrice <= newPrice) {
		console.log(`New max price for ${coin.info.Symbol}: ${newPrice}$`)
		coin.maxPrice = newPrice

		// Buy the crypto if no holding for the crypto and wallet has monney
		if (holdings.find(holding => holding.symbol === coin.info.Symbol) === undefined && wallet > 0) {
			const amount = wallet < 100 ? wallet : 100
			wallet -= amount
			holdings.push({
				symbol: coin.info.Symbol,
				buyPrice: newPrice,
				amount,
			})
		}
	} else {
		// For each holdings
		holdings = holdings.filter((holding, i) => {
			if (holding.symbol !== coin.info.Symbol) {
				return true // Keep holding
			}
			// If newPrice is lower than buyPrice + (maxPrice - buyPrice) * 0.5
			if (newPrice < holding.buyPrice + (coin.maxPrice - holding.buyPrice) * 0.5) {
				// Sell holding
				wallet += holding.amount * (newPrice / holding.buyPrice)
				return false // Remove holding
			}
			return true // Keep holding
		})
	}

	return [wallet, holdings]
}
