export interface ICoinInfo {
	Id: string
	Url: string
	ImageUrl: string
	Name: string
	Symbol: string
	CoinName: string
	FullName: string
	Algorithm: string
	ProofType: string
	FullyPremined: string
	TotalCoinSupply: string
	PreMinedValue: string
	TotalCoinsFreeFloat: string
	SortOrder: string
	Sponsored: boolean
	IsTrading: boolean
}

export interface ICoinList {
	Response: string
	Message: string
	BaseImageUrl: string
	BaseLinkUrl: string
	Data: { [name: string]: ICoinInfo }
	Type: number
}

export interface ICoinHisto {
	time: number
	close: number
	high: number
	low: number
	open: number
	volumefrom: number
	volumeto: number
}

export interface IHistoDay {
	Symbol: string
	Response: string
	Type: number
	Aggregated: boolean
	Data: ICoinHisto[]
	TimeTo: number
	TimeFrom: number
	FirstValueInArray: boolean
	ConversionType: {
		type: string
		conversionSymbol: string
	}
}

export interface IMyCoin {
	info: ICoinInfo
	histo: IHistoDay
	maxPrice: number
}

export interface IHolding {
	symbol: string
	buyPrice: number
	amount: number
}
