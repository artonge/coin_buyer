import fetch from "node-fetch"

import { ICoinList, IHistoDay, IMyCoin, ICoinInfo } from "./models"

export async function getCoins(top: number) {
	const coinlistReponse = await fetch("https://min-api.cryptocompare.com/data/all/coinlist")
	const coinlist: ICoinList = await coinlistReponse.json()
	// Return top 100 coins
	return Object.keys(coinlist.Data)
		.map(coinName => coinlist.Data[coinName])
		.filter(coin => Number.parseInt(coin.SortOrder) <= top)
		.sort((coinA, coinB) => Number.parseInt(coinA.SortOrder) - Number.parseInt(coinB.SortOrder))
}

export async function getCoinHistory(coinInfo: ICoinInfo) {
	const histodayResponse = await fetch(
		`https://min-api.cryptocompare.com/data/histoday?fsym=${coinInfo.Symbol}&tsym=EUR&limit=1`,
	)

	const histoday: IHistoDay = await histodayResponse.json()

	return {
		info: coinInfo,
		histo: histoday,
		maxPrice: histoday.Data.reduce((max, histo) => Math.max(max, histo.high), 0),
	} as IMyCoin
}
