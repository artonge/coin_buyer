import { getCoins, getCoinHistory } from "./cryptocompare_rest_api"
import { bitfinex } from "./bitfinex_web_sockets"

import { IHolding } from "./models"
import { handlePriceChange } from "./lib"

const COIN_NB = 5

let WALLET = 500
let HOLDINGS: IHolding[] = []

// Init and start the deamon
;(async function init() {
	// Get coin list
	const coinlist = await getCoins(COIN_NB)

	// Get history for all coins
	const coins = await Promise.all(coinlist.map(coinInfo => getCoinHistory(coinInfo)))

	// Display coins max price
	// coins.forEach(coin => console.log(`Max price for ${coin.info.Symbol}: ${coin.maxPrice}$`))

	// Subscribe for price changes
	coins.forEach(coin => {
		// Send a subscribe event to the we socket
		// console.log(`Subscribing for ${coin.info.Symbol}...`)
		bitfinex(coin.info.Symbol).subscribe(newPrice => {
			;[WALLET, HOLDINGS] = handlePriceChange(coin, newPrice, WALLET, HOLDINGS)
		})
	})
})()
